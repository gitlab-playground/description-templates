#### Summary

This project is to showcase how to create and use description templates for Issues and Merge Requests

['Feature Proposal' issue template](.gitlab/issue_templates/Feature Proposal.md)
['Bug' issue template](.gitlab/issue_templates/Bug.md)